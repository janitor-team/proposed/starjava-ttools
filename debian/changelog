starjava-ttools (3.4-2) unstable; urgency=medium

  * Add versioned dependencies for starlink-table and -util

 -- Ole Streicher <olebole@debian.org>  Thu, 04 Feb 2021 22:26:11 +0100

starjava-ttools (3.4-1) unstable; urgency=medium

  * New upstream version 3.4. Closes: #980610
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed
  * Add versionized build-deps for starlink-table and -util
  * Add starjava-tjoin as build dep
  * Don't include in/out documents that are not packaged

 -- Ole Streicher <olebole@debian.org>  Sat, 16 Jan 2021 13:40:51 +0100

starjava-ttools (3.3-1) unstable; urgency=medium

  * New upstream version 3.3
  * Rediff patches
  * Remove trailing slashes in d/*.install
  * Add servlet-api dependency

 -- Ole Streicher <olebole@debian.org>  Fri, 06 Nov 2020 13:54:07 +0100

starjava-ttools (3.2.2-1) unstable; urgency=low

  * New upstream version 3.2.2. Rediff patches.
  * Add new dependency from org.jfree.svg and fix its import path

 -- Ole Streicher <olebole@debian.org>  Tue, 22 Sep 2020 13:48:40 +0200

starjava-ttools (3.2.1-1) unstable; urgency=low

  * New upstream version 3.2.1. Rediff patches
  * Push dh compat to 13
  * Add Rules-Requires-Root: no
  * Adjust required versions of other starjava packages
  * Use updated starjava-vo package

 -- Ole Streicher <olebole@debian.org>  Thu, 25 Jun 2020 21:13:48 +0200

starjava-ttools (3.2-2) unstable; urgency=medium

  * Fix permission in jython postinstall script (Closes: #951856)
  * Push Standards-Version to 4.5.0. No changes required

 -- Ole Streicher <olebole@debian.org>  Mon, 24 Feb 2020 17:37:27 +0100

starjava-ttools (3.2-1) unstable; urgency=low

  * New upstream version 3.2. Rediff patches
  * Update minimal version of starlink-util-java and starlink-vo-java
  * Push Standards-Version to 4.4.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Fri, 22 Nov 2019 14:52:55 +0100

starjava-ttools (3.1.6+2019.07.22-2) unstable; urgency=low

  * Adjust min version for starjava-votable build dep

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 14:50:45 +0200

starjava-ttools (3.1.6+2019.07.22-1) unstable; urgency=low

  * New upstream version 3.1.6+2019.07.22. Rediff patches
  * Fix min version for starjava-table
  * Push Standards-Version to 4.4.0. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 12:44:56 +0200

starjava-ttools (3.1.5-1) unstable; urgency=low

  * New upstream version 3.1.5. Rediff patches
  * Push Standards-Version to 4.2.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Tue, 06 Nov 2018 11:48:10 +0100

starjava-ttools (3.1.4-1) unstable; urgency=low

  * Push Standards-Version to 4.1.4. No changes needed.
  * Check DEB_BUILD_OPTIONS on  override_dh_auto_test
  * Adjust required versions for starjava util and vo
  * Add starjava-dpac dependency
  * New upstream version 3.1.4. Rediff patches
  * Temporarily disable linkchecker

 -- Ole Streicher <olebole@debian.org>  Mon, 28 May 2018 11:24:50 -0700

starjava-ttools (3.1.2-2) unstable; urgency=medium

  * Update VCS fields to use salsa.d.o
  * Import java.io in jython package. Closes: #894407
  * Push Standards-Version to 4.1.3. No changes needed.
  * Push compat to 11
  * Catch java.nio.FileNotFoundEx in jython setup
  * Increase tolerance in some tests

 -- Ole Streicher <olebole@debian.org>  Fri, 30 Mar 2018 15:02:14 +0200

starjava-ttools (3.1.2-1) unstable; urgency=low

  * New upstream version 3.1.2
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Wed, 08 Nov 2017 22:13:26 +0100

starjava-ttools (3.1.1-2) unstable; urgency=low

  * Fix jarpath handling to fix CI test

 -- Ole Streicher <olebole@debian.org>  Fri, 06 Oct 2017 11:05:17 +0200

starjava-ttools (3.1.1-1) unstable; urgency=low

  * New upstream version 3.1.1
  * Rediff patches
  * Push Standards-Version to 4.1.1 (no changes)
  * Fix FTBFS against jython-2.7 (Closes: #877397)

 -- Ole Streicher <olebole@debian.org>  Thu, 05 Oct 2017 16:53:35 +0200

starjava-ttools (3.1-2) unstable; urgency=low

  * Add Matplotlib Development Team to d/copyright. Closes: #860077
  * Set HOME in jython-stilts.postinst to some dummy if not set

 -- Ole Streicher <olebole@debian.org>  Tue, 11 Apr 2017 13:19:15 +0200

starjava-ttools (3.1-1) unstable; urgency=low

  * Initial release. (Closes: #855795)

 -- Ole Streicher <olebole@debian.org>  Mon, 10 Apr 2017 21:34:43 +0200
